#include <rip/routines/slam_pose.hpp>

namespace rip::routines
{

    using millimeter_t = units::length::millimeter_t;

    SlamPose::SlamPose(const Position& position)
        : Pose(inch_t(millimeter_t(position.x_mm)),
               inch_t(millimeter_t(position.y_mm)),
               degree_t(position.theta_degrees))
    {
    }

}
