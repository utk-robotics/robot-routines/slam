#ifndef SLAM_POSE_HPP
#define SLAM_POSE_HPP

#include <rip/datatypes/pose.hpp>

#include <Position.hpp>

using rip::datatypes::Pose;

namespace rip::routines
{
    
    class SlamPose : public Pose
    {
        public:
            SlamPose() = default;

            SlamPose(const Position& position);
    };

}

#endif
