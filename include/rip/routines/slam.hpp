#ifndef SLAM_HPP
#define SLAM_HPP

// Global
#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

// External
#include <BreezySLAM.hpp>
#include <nlohmann/json.hpp>

// Local
//#include "rip/callback.hpp"
#include "rip/routine.hpp"
#include "rip/dab/lidar.hpp"
#include "rip/routines/slam_pose.hpp"

namespace rip::routines
{

    NEW_RIP_EX(InvalidComponentName);
    NEW_RIP_EX(ComponentNotFound);
    NEW_RIP_EX(LidarReadTimeout);
    /**
     * @class Slam
     * @brief Wrapper that connects the lidar and the SLAM algorithms
     */
    class Slam : public Routine
    {
    public:
        /**
         * @brief Constructor
         *
         * @param lidar The lidar used
         * @param config The configuration for the slam algorithms
         */
        Slam(const nlohmann::json& config, std::shared_ptr<EventSystem> es, 
                std::string id, CompTable comps);

        /**
         * @brief Starts the background thread which computes the robot's
         * position
         */
        virtual void start(std::vector<std::any> data = {}) override;

        /**
         * @brief Stops the background thread which computes the robot's
         * position
         */
        virtual void stop(std::vector<std::any> data = {}) override;

        // Depreciated
        /**
         * @brief Adds a callback function that is triggered when a new pose is
         * computed
         *
         * @param callback The function to trigger
         */
        //void addCallback(const std::function< void(Pose) >& callback);

        /**
         * @brief Returns the pose of the robot
         *
         * @returns The pose of the robot
         */
        Pose get();

    protected:

        virtual void saveComponents(CompTable comps) override;

        /**
         * @brief Functions which runs in a background thread computing the
         * robot's position through slam
         */
        virtual void run() override;

        virtual void handle_subscription(std::string handle, std::string func_id, 
                std::string routine_id="") override;

    private:
        // Depreciated
        /**
         * @brief The callback used to collect the lidar scans
         *
         * @param scan The scan received from the lidar
         */
        //void lidarCallback(const std::vector< LaserScan >& scan);

        std::shared_ptr< Lidar > m_lidar;
        std::unique_ptr< RMHC_SLAM > m_slam;
        Laser m_laser;

        // Threading is controlled by EventSystem
        //std::unique_ptr< std::thread > m_thread;
        // Provided by AbstractRoutine
        //std::atomic< bool > m_running;
        std::mutex m_mutex;
        std::mutex m_pose_mutex;

        std::vector< LaserScan > m_last_scan;
        SlamPose m_pose;

        double m_time_validation;

        //Callback< Pose > m_callbacks;
    };
}  // namespace rip

#endif  // SLAM_HPP
